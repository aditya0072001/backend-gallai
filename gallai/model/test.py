import tensorflow as tf
import tensorflowjs as tfjs
from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg19 import preprocess_input
from tensorflow.keras.models import Model
from tensorflow.keras.models import load_model
import numpy as np

# other imports
import json
import datetime
import time
import glob
import os

clases = ['Memes', 'Person', 'Screenshots']
#file = "Data/Memes/5c0v6z.jpg"
file = "Data/Person/David_Caruso_0002.jpg"
#file = "Data/Person/Alex_Barros_0001.jpg"
model = load_model("model.h5")
img = image.load_img(file, target_size=(224, 224))
x = image.img_to_array(img)
x = np.expand_dims(x, axis=0)
x = preprocess_input(x)
y_prob = model.predict(x)
y_class = y_prob.argmax(axis=-1)
y_class = y_class[0]
y_confidence = int(y_prob[0][y_class] * 100)

print(y_class,y_confidence,y_prob)

print(clases[np.argmax(y_prob)])

tfjs.converters.save_keras_model(model, "tfjs_files")