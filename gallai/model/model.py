import tensorflow as tf
from tensorflow import keras
import numpy as np
from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Dense, GlobalAveragePooling2D, Input
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.callbacks import ModelCheckpoint
import json
import datetime
import time


data_dir = "Data"
batch_size = 32
img_height = 224
img_width = 224
epochs = 10

train_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="training",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)


val_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="validation",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)

classes = train_ds.class_names

base_model = MobileNetV2(include_top=False,
                            input_tensor=Input(shape=(224,224,3)), input_shape=(224,224,3))
top_layers = base_model.output
top_layers = GlobalAveragePooling2D()(top_layers)
top_layers = Dense(1024, activation='relu')(top_layers)
predictions = Dense(len(classes), activation='softmax')(top_layers)
model = Model(inputs=base_model.input, outputs=predictions)

print(train_ds,val_ds)
print(classes,len(classes))

start = time.time()

"""print ("Freezing the base layers. Unfreeze the top 2 layers...")
for layer in model.layers[:-3]:
    layer.trainable = False
"""

model.compile(optimizer='rmsprop', loss='sparse_categorical_crossentropy', metrics=['accuracy'])


model.fit(train_ds, validation_data=val_ds,epochs=epochs,steps_per_epoch=batch_size, verbose=1)

print ("Saving...")
model.save("model.h5") 
