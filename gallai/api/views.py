from django.shortcuts import render
from django.core import serializers
from .serializers import UserSerializer,ImagesSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAdminUser,IsAuthenticated
from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from .models import Images
from rest_framework.parsers import JSONParser,MultiPartParser,FormParser
import tensorflow as tf
from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg19 import preprocess_input
from tensorflow.keras.models import Model
from tensorflow.keras.models import load_model
import numpy as np
import json
import datetime
import time
import glob
import os

class UserRecordView(APIView):
    """
    API View to create or get a list of all the registered
    users. GET request returns the registered users whereas
    a POST request allows to create a new user.
    """
    permission_classes = [IsAdminUser]

    def get(self, format=None):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid(raise_exception=ValueError):
            serializer.create(validated_data=request.data)
            return Response(
                serializer.data,
                status=status.HTTP_201_CREATED
            )
        return Response(
            {
                "error": True,
                "error_msg": serializer.error_messages,
            },
            status=status.HTTP_400_BAD_REQUEST
        )

class UploadImages(APIView):

    authetication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    paser_classes = [JSONParser,MultiPartParser,FormParser]

    def post(self,request):
    	print(request.data)
# 		request.data.owner = user.email'
    	try:
    		print(Images.objects.get(img=request.data.img))
    		if Images.objects.get(img=request.data.img) != None:
    			return Response({"Image already exist"},status=400)
    	except Exception as e:
    		print(e.__class__)
    		request.data["owner"] = request.user.email
    		print(request.data)
    	serializer = ImagesSerializer(data = request.data)
    	if serializer.is_valid():
    		serializer.save()
    		return Response(serializer.data,status=200)
    	else:
    		return Response(serializer.errors,status=400)

class ImagesView(APIView):
    authetication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    paser_classes = [JSONParser,MultiPartParser,FormParser]

    def get(self,request):
    	data=Images.objects.filter(owner=request.user.email)
    	serializer = ImagesSerializer(data, many=True)
    	return Response(serializer.data,status=200)

class ClassifyImages(APIView):
    authetication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    paser_classes = [JSONParser,MultiPartParser,FormParser]

    def post(self,request):
    	dataImg=Images.objects.filter(owner=request.user.email)
    	images = []
    	for img in list(dataImg.values()):
    	   images.append(img["img"])
#    	print(serializers.serialize('json',dataImg))
#    	request.data=dataImg.values()
#    	request.data.setdefault(serializers.serialize('json',dataImg))
    	clases = ['Memes', 'Person', 'Screenshots']
    	model = load_model("model/model.h5")
    	predictions = []
    	for h in images:
        		img = image.load_img(h, target_size=(224, 224))
        		x = image.img_to_array(img)
        		x = np.expand_dims(x, axis=0)
        		x = preprocess_input(x)
        		prediction = model.predict(x)
        		prediction = clases[np.argmax(prediction)]
        		predictions.append(prediction)


    	for da,prediction in zip(dataImg.values(),predictions):
        		da["is_category"] = prediction
#        		print(da["is_category"])
 #   	print(dataImg.values())
    	for d,i in zip(predictions,images):
    	    dt = Images.objects.get(img=i)
    	    dt.is_category = d
    	    dt.save()
#    	dataImg.bulk_update(prediction,['is_category'])
    	return Response(serializers.serialize('json',dataImg),status=200)
"""
    	serializer = ImagesSerializer(data=list(dataImg.values()), many=True)
    	if serializer.is_valid():
        	clases = ['Memes', 'Person', 'Screenshots']
        	model = load_model("model/model.h5")
        	predictions = []
        	for h in serializer.data:
        		img = image.load_img(h["img"], target_size=(224, 224))
        		x = image.img_to_array(img)
        		x = np.expand_dims(x, axis=0)
        		x = preprocess_input(x)
        		prediction = model.predict(x)
        		prediction = clases[np.argmax(prediction)]
        		predictions.append(prediction)


        	for da,prediction in zip(serializer.data,predictions):
        		da["is_category"] = prediction
        		print(da["is_category"])

        	print(serializer.data)
        	serializer.save()
        	return Response(serializer.data,status=200)
    	else:
        	return Response(serializer.errors,status=400)
"""


class DeleteImage(APIView):
    authetication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    paser_classes = [JSONParser,MultiPartParser,FormParser]

    def post(self,request):
#    	print(str(list(request.data.values())[0].split("media")[1][1:]))
#    	print(list(request.data.values())[0].split("media")[1])
#    	print(Images.objects.get(img = str(list(request.data.values())[0].split("media")[1])))
    	#print(Images.objects.get(img ="images/George_W_Bush_0144.jpg"))
    	img_delete = Images.objects.get(img = list(request.data.values())[0].split("media")[1][1:])
    	img_delete.delete()
    	os.remove(list(request.data.values())[0].split("media")[1][1:])
    	return Response("Success",status=200)

class CheckImage(APIView):
    authetication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    paser_classes = [JSONParser,MultiPartParser,FormParser]

    def get(self,request):
        if Images.objects.filter(owner = request.user.email).exists():
            return Response("Exists",status=200)
        else:
            return Response("Doesn't Exists",status=404)
