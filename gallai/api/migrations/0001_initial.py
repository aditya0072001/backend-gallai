# Generated by Django 3.0.5 on 2021-03-21 11:43

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Images',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('is_category', models.CharField(max_length=20)),
                ('owner', models.CharField(max_length=200)),
            ],
        ),
    ]
