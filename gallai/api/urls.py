from django.urls import path
from .views import UserRecordView,UploadImages,ImagesView,ClassifyImages,DeleteImage,CheckImage
from django.conf.urls.static import static
from django.conf import settings

app_name = 'api'

urlpatterns = [
    path('user/', UserRecordView.as_view(), name='users'),
    path('uploadimage/',UploadImages.as_view(),name='uploadimage'),
    path('imagesview/',ImagesView.as_view(),name='ImagesView'),
    path('classifyimages/',ClassifyImages.as_view(),name='ClassifyImages'),
    path('deleteimage/',DeleteImage.as_view(),name='DeleteImage'),
    path('checkimage/',CheckImage.as_view(),name='CheckImage')
]